

class ExceptionInternal(Exception):
    def __init__(self, mensaje="Ocurrió un error personalizado", codigo=500):
        self.mensaje = mensaje
        self.codigo = codigo
        super().__init__(self.mensaje)