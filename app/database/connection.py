import psycopg2

# ================================= #
#        C O N N E C T I O N        #
# ================================= #


def connection_bd(_host, _port, _database, _user, _password):
    try:            
        connection = psycopg2.connect(
            host=_host,
            port=int(_port),
            database=_database,
            user=_user,
            password=_password
        )
        
        db_info = connection.get_dsn_parameters()
        print(f"Conexion exitosa a la base de datos: {db_info['dbname']}")
        print(f"Datos adicionales: {db_info}")
        
        return connection
    
    except Exception as ex:
        print('Error en connection_bd: ', ex)
        return None