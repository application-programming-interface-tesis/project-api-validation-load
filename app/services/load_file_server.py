import io
import re
import os
import pytz
import shutil
import unicodedata
from pathlib import Path
from datetime import datetime
from fastapi import UploadFile, HTTPException
from fastapi.responses import JSONResponse
from app.config.config import Config
from app.services.configuration_file import get_dataframe

config = Config()
desired_timezone = pytz.timezone('America/Guayaquil')

def validate_extension_file(file_name: str):
    permitted_extensions = {'.csv', '.txt', '.xls', '.xlsx'}
    file_extension = Path(file_name).suffix.lower()
    print('file_extension: ', file_extension)
    
    return file_extension in permitted_extensions

    
async def load_file_server_etl(file_name: str, file: UploadFile):
    try:
        validation_result = validate_extension_file(file_name)
        if not validation_result:
            return JSONResponse(status_code=400, content={"code": 1, "message": f"La extensión del archivo {file.filename} no es permitida"})
        
        file_path = os.path.join(config.PATH_UPLOAD_FOLDER, file_name)
        
        with open(file_path, "wb") as f:
            f.write(file.file.read())
        
        return JSONResponse(status_code=200, content={"code": 0, "message": f"Archivo [{file_name}] transferido con exito"})
    
    except HTTPException as ex:
        print('Error HTTPException en load_file_server_etl: ', ex)
        return JSONResponse(status_code=ex.status_code, content={"code": 1, "message": f"Error en load_file_server_etl - {ex.detail}"})
    
    except Exception as ex:
        print('Error en load_file_server_etl: ', ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error en load_file_server_etl - {ex}"})
    
    
def load_file_copy_server_etl(file: UploadFile):
    try:
        print('inicio')
        validation_result = validate_extension_file(file.filename)
        if not validation_result:
            return JSONResponse(status_code=400, content={"code": 1, "message": f"La extensión del archivo {file.filename} no es permitida"})
            
        print('paso....')
        file_path = os.path.join(config.PATH_UPLOAD_FOLDER, file.filename)
        print('file_path: ', file_path)
        with open(file_path, 'wb') as buffer:
            shutil.copyfileobj(file.file, buffer)
        
        return JSONResponse(status_code=200, content={"code": 0, "message": f"Archivo [{file.filename}] transferido con exito"})
        
    except Exception as ex:
        print('Error en load_file_save_server_etl: ', ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error en load_file_server_etl - {ex}"})
     

# ============================================ #
#   G E T   I N F O R M A T I O N    F I L E   #
# ============================================ #


def extract_date(file_name):
    # Patrón para el formato ENE2024
    pattern1 = r"_(\w{3})(\d{4})$"
    match1 = re.search(pattern1, file_name)
    
    # Patrón para el formato 02042023
    pattern2 = r"_(\d{2})(\d{2})(\d{4})$"
    match2 = re.search(pattern2, file_name)
    
    # Patrón para el formato 052024
    pattern3 = r"_(\d{2})(\d{4})$"
    match3 = re.search(pattern3, file_name)
    
    if match1:
        month = match1.group(1)
        year = match1.group(2)
        date = f"{month}{year}"
        pattern = "BBYYYY"
    elif match2:
        day = match2.group(1)
        month = match2.group(2)
        year = match2.group(3)
        date = f"{day}{month}{year}"
        pattern = "DDMMYYYY"
    elif match3:
        month = match3.group(1)
        year = match3.group(2)
        date = f"{month}{year}"
        pattern = "MMYYYY"
    else:
        date = None
        pattern = None
    
    return {"date": date, "pattern": pattern}


def detect_date_format(str_date):
    pattern_ddmmyyyy = r'^\d{8}$'
    pattern_mmyyyy = r'^\d{6}$'
    pattern_bbyyyy = r'^[A-Z]{3}\d{4}$'
    
    if re.match(pattern_ddmmyyyy, str_date):
        return "DDMMYYYY"
    elif re.match(pattern_mmyyyy, str_date):
        return "MMYYYY"
    elif re.match(pattern_bbyyyy, str_date):
        return "BBYYYY"
    else:
        return None


def remove_date_filename(file_name):
    pattern_ddmmyyyy = r"\d{6}$"  
    pattern_mmmyyyy = r"(ENE|FEB|MAR|ABR|MAY|JUN|JUL|AGO|SEP|OCT|NOV|DIC)\d{4}$"  
    pattern_mmyyyy = r"\d{4}(0[1-9]|1[0-2])$" 

    match_ddmmyyyy = re.search(pattern_ddmmyyyy, file_name)
    match_mmmyyyy = re.search(pattern_mmmyyyy, file_name)
    match_mmyyyy = re.search(pattern_mmyyyy, file_name)

    if match_ddmmyyyy:
        new_file_name = re.sub(pattern_ddmmyyyy, '', file_name)
        return new_file_name
    elif match_mmmyyyy:
        new_file_name = re.sub(pattern_mmmyyyy, '', file_name)
        return new_file_name
    elif match_mmyyyy:
        new_file_name = re.sub(pattern_mmyyyy, '', file_name)
        return new_file_name
    else:
        return None  


async def get_information_file_etl(file: UploadFile):
    try:
        detail_erros = []
        
        filename_without_extension, extension_name = os.path.splitext(file.filename)
        file_extension = extension_name[1:]
        
        try:
            filename_without_ext = str(filename_without_extension).split('_')
            file_date_format = filename_without_ext[-1]
            file_name_without_date = "_".join(filename_without_ext[:-1])
        except Exception as e:
            print('Error:  ', e)
            file_date_format = None
            file_name_without_date = None
            
        if file_date_format is not None:
            file_date_format_pattern = detect_date_format(file_date_format)
        else:
            file_date_format_pattern = None
        
        if file_name_without_date is None:
            detail_erros.append(f"El archivo {file.filename} debe contener la fecha")
        
        if file_date_format is None or file_date_format_pattern is None:
            detail_erros.append(
                "Formato de fecha incorrecto, solo se permite los formatos DDMMYYYY, MMYYYY, BBYYYY")

        if not validate_extension_file(str(file.filename)):
            detail_erros.append(f"El archivo contiene un formato {file_extension} no permitido, solo se permite csv, xlsx, xls, txt")
        
        try:
            column_names = []
            file_content = await file.read()  
            file_buffer = io.BytesIO(file_content) 
            df = get_dataframe(file_buffer, file_extension)
            column_names = df.columns.tolist()
            
            for column_name in column_names:
                if not column_name.isupper():
                    detail_erros.append(f"El nombre de columna '{column_name}' no está en mayúsculas.")
                if ' ' in column_name:
                    detail_erros.append(f"El nombre de columna '{column_name}' debe estar separado por subguiones '_' en lugar de espacios.")
                if any(unicodedata.category(char) == 'Mn' for char in column_name):
                    detail_erros.append(f"El nombre de columna '{column_name}' no debe contener tildes.")
            
        except Exception as pe:
            print('Error: ', pe)
            column_names = []
                    
        if not column_names:
            detail_erros.append(f"No posee cabecera de columnas el archivo {file_extension}")
            
        information = {
            "file_name": file.filename,
            "file_name_without_extension": filename_without_extension,
            "file_name_without_date": file_name_without_date,
            "file_date_format": file_date_format,
            "file_date_format_pattern": file_date_format_pattern,
            "file_extension": file_extension,
            "file_content_type": file.content_type,
            "file_size": file.size,
            "file_columns": column_names,
            "detail_approved": {
                "approved_file": True if not detail_erros else False,
                "detail_erros": detail_erros
            }
        }
        rsp = {"code": 0,
               "message": "Exito",
               "content": information}

        return JSONResponse(status_code=200, content=rsp)

    except Exception as ex:
        print('Error en get_information_file_etl: ', ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error en get_information_file_etl - {ex}"})


async def get_list_information_file_etl():
    try:
        if not os.path.isdir(config.PATH_UPLOAD_FOLDER):
            return JSONResponse(status_code=500, content={"code": 1, "message": f"La ruta {config.PATH_UPLOAD_FOLDER} no es un directorio válido", "content": []})

        files_list = os.listdir(config.PATH_UPLOAD_FOLDER)
        if not files_list:
             return JSONResponse(status_code=404, content={"code": 1, "message": f"Directorio {config.PATH_UPLOAD_FOLDER} se encuentra vacio", "content": []})

        files_info = []
        for file_name in files_list:
            file_path = os.path.join(config.PATH_UPLOAD_FOLDER, file_name)
            #HORA Y FECHA DEL ARCHIVO
            file_modified_timestamp = os.path.getmtime(file_path)
            file_modified_datetime = datetime.fromtimestamp(file_modified_timestamp)
            file_modified_datetime_with_timezone = file_modified_datetime.astimezone(desired_timezone)
            file_modified_date = file_modified_datetime_with_timezone.strftime("%d-%m-%Y %H:%M:%S")
            
            #file_modified_date = datetime.fromtimestamp(os.path.getmtime(file_path)).strftime("%d-%m-%Y %H:%M:%S")

            filename_without_extension, extension_name = os.path.splitext(file_name)

            try:
                filename_without_ext = str(filename_without_extension).split('_')
                file_name_without_date = "_".join(filename_without_ext[:-1])

            except Exception as e:
                print('Error: ', e)
                file_name_without_date = None

            file_info = {
                "file_name": file_name,
                "file_alias": file_name_without_date,
                "file_upload_date": file_modified_date
            }

            files_info.append(file_info)

        rsp = {"code": 0,
               "message": "Exito",
               "content": files_info}

        return JSONResponse(status_code=200, content=rsp)

    except Exception as ex:
        print('Error en get_list_information_file_etl: ', ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error en get_list_information_file_etl - {ex}"})


def remove_list_file_name_etl(name_file: str):
    try:
       path_file = os.path.join(config.PATH_UPLOAD_FOLDER, name_file)

       if os.path.exists(path_file):
           os.remove(path_file)
           rsp = {"code": 0,
                  "message": f"Archivo {name_file} eliminado con éxito"}

           return JSONResponse(status_code=200, content=rsp)
       else:
           return JSONResponse(status_code=404, content={"code": 1, "message": f"No existe el archivo {name_file} en la ruta del servidor"})
              
    except Exception as ex:
        print('Error en remove_list_file_name_etl: ', ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error en remove_list_file_name_etl - {ex}"})
    