from fastapi.responses import JSONResponse
from app.models.request import TypeRule


def get_information_type_rules_etl(type_rule: TypeRule):
    try:
        list_rules = []
        if type_rule == 'B':
            list_rules = [
                {'name_rule': 'Validación de Tamaño', 'alias_rule': 'VAL_TAM'},
                {'name_rule': 'Validación de Texto', 'alias_rule': 'VAL_TXT'},
                {'name_rule': 'Validación de Números', 'alias_rule': 'VAL_NUM'},
                {'name_rule': 'Validación de Fecha', 'alias_rule': 'VAL_FEC'},
                {'name_rule': 'Validación Alfanumérica', 'alias_rule': 'VAL_ALF'},
                {'name_rule': 'Validación de Números Negativos', 'alias_rule': 'VAL_NEG'},
                {'name_rule': 'Validación de Duplicados', 'alias_rule': 'VAL_DUP'}
            ]
            print(list_rules)

        else:
            if type_rule == 'E':
                list_rules = [
                    {'name_rule': 'Valor Requerido', 'alias_rule': 'required_value'},
                    {'name_rule': 'Valor Inicial', 'alias_rule': 'start_value'},
                    {'name_rule': 'Valor Inválido', 'alias_rule': 'invalid_value'}
                ]
                print(list_rules)
        
        rsp = {
            "code": 0, 
            "message": "Exito",
            "content": list_rules
            }
        print(rsp)        
        return JSONResponse(status_code=200, content=rsp)
    
    except Exception as ex:
        print('Error en get_rules_name: ', ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"{str(ex)}"})