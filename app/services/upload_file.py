#==================================#
#        L I B R E R I A S         #
#==================================#
import pytz
import json
import logging
import pandas as pd
from fastapi.responses import JSONResponse
from datetime import datetime
from app.config.config import Config
from app.database.connection import connection_bd
from app.middlewares.excepciones import ExceptionInternal
from app.util.functions import (convert_df_to_csv, 
                                delete_file_csv, 
                                delete_table_temporary, 
                                execute_file_pgload, 
                                get_dataframe, 
                                get_file_date_format, 
                                get_file_name_extension, 
                                move_backup_file, 
                                remove_file, 
                                remove_line_breaks, 
                                update_header_dataframe, 
                                update_log_file, 
                                validate_file_path)

ANIO_MES_DIA_HHMMSS = "%Y-%m-%d %H:%M:%S"
YYYYMMDD = "%Y%m%d"
YYYY_MM_DD = "%Y-%m-%d"

config = Config()
horario_local = datetime.now(pytz.timezone('America/Guayaquil'))
#==================================#
#        F U N C I O N E S         #
#==================================#

def generate_date_field(fecha, det_reproceso):
    try:
        f_beh_date = det_reproceso['field_behavior_date']
        if f_beh_date == 'DIA_INI_MES': 
            return fecha[:4] + fecha[4:6] + '01'
        elif f_beh_date == 'DIA_FIN_MES':
            #RETORNA FECHA QUE INGRESO DESDE CONTROL-M
            return fecha
        elif f_beh_date == 'DIA_INI_FIN_MES':          
            #RETORNA FECHA CON EL PRIMER Y ULTIMO DIA DEL MES            
            return  fecha[:4] + fecha[4:6] + '01' + chr(124) + fecha
    except Exception as ex:
        print('Error en generacion de fecha: ', ex)
        logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Error en generacion de fecha: ' + str(ex))
        return None


def get_value_field_date(v_date_tmp, config_campo_fecha, detalle_fecha_repro):
    try:
        list_field_name_date = config_campo_fecha['list_field_name_date']
        field_date_ax = detalle_fecha_repro['field_name_date']
        #CONSULTO SI EXISTE EL | "PAI" DENTRO DE LA CADENA FECHA
        if v_date_tmp.count(chr(124)) > 0:               
            fecha_tmp_list = v_date_tmp.split(chr(124))            
            df_fnm_date = pd.DataFrame(list_field_name_date)   
            for idx, rw in df_fnm_date.iterrows():                
                if field_date_ax == rw.name_field:
                    v_date_tmp = fecha_tmp_list[idx]
            return v_date_tmp
        else:
            return v_date_tmp                    
    except Exception as ex:
        print('Error al obtener valor del campo fecha: ', ex)
        logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Error al obtener valor del campo fecha: ' + str(ex)) 
        return None



def add_field_df(session, df_tbl, db, schema, name_table):
    try:
        cursor = session.cursor()
        query_cant_field = f"""
        SELECT count(column_name) as total_field_table
            FROM information_schema.columns
            WHERE table_schema = '{schema}'
            AND table_name = '{name_table.lower()}'
            AND table_catalog = '{db}'    
        """
        print(query_cant_field)
        cursor.execute(query_cant_field)
        rsp_cant = (cursor.fetchone())[0]
        cant_df = df_tbl.shape[1]
        if rsp_cant > cant_df:
            diferencia = rsp_cant - cant_df
            for i in range(diferencia):
                cnt_ax_t = df_tbl.shape[1]             
                df_tbl.insert(cnt_ax_t, 'FIELD_AUX_'+ str((i+1)), '', allow_duplicates=False)
            print('Nuevo Df: \n', df_tbl)
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Nuevo Df: \n' + str(df_tbl))
            return df_tbl
        else:
            return df_tbl
    except Exception as ex:
        print('Error en agregar campo fecha en Df: ', ex)
        logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Error en agregar campo fecha en Df: ' + str(ex))
        return None


def add_value_date_field(fecha, f_date , det_reproceso, df_load, lst_nm_campo_fecha):
    try:                       
        f_beh_date = det_reproceso['field_behavior_date']
        if f_beh_date in ['DIA_INI_MES', 'DIA_FIN_MES']:              
            date_load = datetime.strptime(fecha, YYYYMMDD).strftime(YYYY_MM_DD)
            df_load.insert(0, f_date, date_load, allow_duplicates=False)
        else:                    
            list_fecha = fecha.split(chr(124))            
            df_cfg_date = pd.DataFrame(lst_nm_campo_fecha)            
            for idx, rw in df_cfg_date.iterrows():                                        
                idx_date_load = datetime.strptime(list_fecha[idx], YYYYMMDD).strftime(YYYY_MM_DD)
                df_load.insert(rw.field_position, rw.name_field, idx_date_load, allow_duplicates=False) 
        return df_load        
    except Exception as ex:
        print('Error en generar valor para campo(s) de fecha: ', ex)
        logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Error en generar valor para campo(s) de fecha: ' + str(ex))
        return None 


def get_format_field(d_field, d_type, d_length):
    try:
        new_format_field =""
        d_length = str(d_length).replace('.', ',')
        if 'INTEGER' == d_type:
            new_format_field = f'CAST({d_field} AS INTEGER) AS {d_field}'          
        elif 'NUMERIC' == d_type:
            new_format_field = f"CAST(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CAST({d_field} AS VARCHAR), '-',''), '$',''), '(',''), 'NO APLICA',''), 'PRUEBA',''), 'NO DATA',''), ' ',''), ',','.') AS NUMERIC({d_length})) AS {d_field} "
        elif 'DATE' == d_type:
            new_format_field = f"{d_field}::date AS {d_field}"            
        elif 'DOUBLE PRECISION' == d_type:
            new_format_field = f"CAST(REPLACE({d_field},',', '.') AS DOUBLE PRECISION) AS {d_field}"                
        else:
            new_format_field = d_field        
        return new_format_field
    except Exception as ex:
        print('Error en formato del campo: ', ex)
        logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Error en formato del campo: ' + str(ex))
        return None


def execute_reprocess(session, fecha, tabla_final, nm_fdate, f_date_format) -> int:
    try:
        date_load = datetime.strptime(fecha, YYYYMMDD).strftime(YYYY_MM_DD)
        print('Iniciando reproceso de la tabla final ', tabla_final ,' para la fecha: ', fecha)        
        print(f"DELETE FROM {tabla_final} WHERE {nm_fdate}::date = TO_DATE('{date_load}' , '{f_date_format}')")
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + " :: Se ejecuta el Delete." )        
        
        query_reprocess = f"""
            DELETE FROM {tabla_final} WHERE {nm_fdate}::date = TO_DATE('{date_load}' , '{f_date_format}')
        """
        cursor = session.cursor()
        cursor.execute(query_reprocess)
        session.commit()
        return 0
 
    except Exception as ex:
        print('Error en reproceso: ', ex)
        logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Error en reproceso: ' + str(ex))
        return 1


def modify_temporary_table(config_table, table_tmp, table_final):
    list_temp = []    
    insert_temp =""
    select_temp =""
    lst_nm_tbl_tmp = config_table['columns_name'].copy()
    reglas_tmp = config_table['rules_validations'].copy()    
    add_field_date = config_table['config_field_date']['add_date_field']    
    data_nm_field = config_table['config_field_date']['list_field_name_date']    
    try:              
        df_data_field = pd.DataFrame(data_nm_field)            
        #SE AGREGA EL CAMPO FECHA SI SU VALOR ES TRUE
        if add_field_date:
            #OBTENGO EL NUMERO DE FILAS
            if df_data_field.shape[0] > 0:                      
                for idx, rw in df_data_field.iterrows():
                    nombre_campo_fecha = rw.name_field
                    posicion_campo_fecha = rw.field_position                                
                    lst_nm_tbl_tmp.insert(posicion_campo_fecha, nombre_campo_fecha)
        #AMAR INSERT
        for c_insert in lst_nm_tbl_tmp:
            insert_temp = insert_temp + c_insert + chr(44) + chr(32)
        insert_temp = insert_temp[ 0 : len(insert_temp) - 2]
        insert_temp = 'INSERT INTO ' + table_final + chr(40) + insert_temp + chr(41)
        #OBTENGO LISTA DE DATOS DE FORMATO DE CADA COLUMNA
        for dic_reglas in reglas_tmp:
            if 'rules' in dic_reglas:
                del dic_reglas['rules']
                list_temp.append(dic_reglas)                                                
        #GENERO DF CON DATOS DE FORMATO DE CADA COLUMNA             
        df_data = pd.DataFrame(list_temp)     
        #ARMAR SELECT CON FORMATO DE CADA CAMPO
        for field in lst_nm_tbl_tmp:
            #CONSULTAR SI EXISTE EL CAMPO EN LA LISTA DE CONFIGURACION
            if field in df_data.column_validate.tolist():            
                for idx, rw in df_data.iterrows():
                    if field == rw.column_validate:
                        #OBTENER CAMPO CON FORMATO DE ACUERDO A LO CONFIGURADO
                        campo_aux = get_format_field(field, rw.data_type, rw.length)
                        select_temp = select_temp + campo_aux + chr(44) + chr(32)
            else:
                lst_field_pivot_date = ['FECHA_MAPEO', 'FECHA_PROCESO', 'FECHA_CARGA', 'FECHA_INICIO', 'FECHA_FIN', 'FECHA_VIGENCIA']
                if field in lst_field_pivot_date:
                    campo_aux = f"{field}::date AS {field}" 
                    select_temp = select_temp + campo_aux + chr(44) + chr(32)
 
        select_temp = select_temp[ 0 : len(select_temp) - 2]
        select_temp = 'SELECT ' + select_temp + ' FROM ' + table_tmp
        table_temporary_aux = insert_temp + chr(32) + select_temp 
        return table_temporary_aux
    except Exception as ex:
        print('Error en la generacion de formato de insert de la tabla temporal: ', ex)
        logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Error en la generacion de formato de insert de la tabla temporal: ' + str(ex))        
        return None



def load_file_table(fecha, ruta_archivo, ext_archivo, archivo, r_dat, det_tabla, det_reproceso, cfg_, cfg_f_fecha, cab_archivo, delimitador) -> JSONResponse:
    try:
        file_csv = archivo + '.csv'
        file_load_csv = r_dat + file_csv       
        alias_tmp = config.ALIAS_TMP + chr(95)
        BASE = config.BD_DATABASE
        ESQUEMA = det_tabla['scheme']
        field_date = det_reproceso['field_name_date']
        field_date_format = det_reproceso['field_date_format']
        tabla_carga = det_tabla['name_table'].upper()
        lista_nm_campo_fecha = cfg_f_fecha['list_field_name_date'] 
        tmp_table_storage = config.TMP_SCHEME + chr(46) + alias_tmp + tabla_carga
        tabla_final = ESQUEMA + chr(46) + tabla_carga
        
        with connection_bd(config.BD_HOST, config.BD_PORT, config.BD_DATABASE, config.BD_USER, config.BD_PASSWORD) as session:
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Conectado exitosamente a la base de datos')
            
            #DROPEO DE TABLA TMP
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Dropeo de la tabla temporal ' + tmp_table_storage)
            print('Eliminando tabla temporal... ', tmp_table_storage) 
            cursor = session.cursor()
            cursor.execute(f"DROP TABLE IF EXISTS {tmp_table_storage}")
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Query a ejecutar : ' + f"DROP TABLE IF EXISTS {tmp_table_storage}")

            #CREACION DE TABLA TMP
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Creacion de tabla temporal ' + tmp_table_storage)
            print('Creando tabla temporal... ', tmp_table_storage)
            query_create = f"CREATE TABLE {tmp_table_storage} AS SELECT * FROM {tabla_final} WHERE 1 = 0"
            cursor.execute(query_create)
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Query a ejecutar: ' + query_create)
            
            #GENERAR DATAFRAME
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Generar Dataframe')
            print('Generar Dataframe')
            df_data_load_tmp = get_dataframe(ruta_archivo, ext_archivo, cab_archivo, delimitador)
            print(df_data_load_tmp)
            
            #ACTUALIZAR ENCABEZADOS DE LOS NOMBRES DE LAS COLUMNAS
            print('Actualizar encabezados de los nombres de las columnas')
            df_data_load_tmp = update_header_dataframe(df_data_load_tmp.columns.to_list(), df_data_load_tmp)
            print(df_data_load_tmp.columns)
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Dataframe con nombre de columnas actualizadas: \n' + str(df_data_load_tmp.columns))
            
            #OBTENER EL CAMPO PARA REPROCESO
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Obtener campo para reproceso en la tabla final ' + tabla_carga)
            print('Obtener campo para reproceso en tabla final ', tabla_carga)
            
            if len(field_date) > 0:
                df_data_load = add_value_date_field(fecha, field_date, det_reproceso, df_data_load_tmp, lista_nm_campo_fecha)
                print('Nuevo DF con campos adicionales: \n', str(df_data_load)) 
            
            df_data_load = add_field_df(session, df_data_load, BASE, ESQUEMA, tabla_carga)
            print(df_data_load_tmp.dtypes)
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Tipos de datos del Dataframe: \n' + str(df_data_load_tmp.dtypes))
            
            #ELIMINAR LOS SALTOS DE LINEA
            df_data_load = remove_line_breaks(df_data_load)
            
            #CONVERSION A CSV Y GUARDADO EN LA RUTA
            print('Se procede a generar .CSV para cargar a base')
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Se procede a generar .CSV para cargar a base') 
            convert_df_to_csv(df_data_load, file_load_csv)
            
            #REALIZAR CARGA DEL CSV A LA TABLA TMP
            print('Iniciando carga de archivo a tabla temporal ', tmp_table_storage)
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Iniciando carga de archivo a tabla temporal')
            rsp_func_load = execute_file_pgload(session, file_load_csv, tmp_table_storage)
            print("Respuesta de funcion de carga a BD: " + str(rsp_func_load))    
    
            if rsp_func_load > 0:
                raise ExceptionInternal(codigo=403, mensaje=f'No se puede cargar el archivo: {file_csv} a la tabla temporal {tmp_table_storage}')
            
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Archivo cargado con exito a tabla temporal ' + tmp_table_storage)
            print(f'Archivo cargado con exito a tabla temporal {tmp_table_storage}')
            
            #REALIZAR REPROCESO ANTES DE INSERTAR DATOS
            print(f'Iniciando reproceso de la tabla {tabla_final}')
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Iniciando reproceso de la tabla: ' + tabla_final)
            rsp_reproceso = execute_reprocess(session, fecha, tabla_final, field_date, field_date_format)
            
            if rsp_reproceso > 0:
                raise ExceptionInternal(codigo=403, mensaje=f'Error al realizar el reproceso {rsp_reproceso}')
            
            # INSERTAR DATOS DE TBL_TMP A LA TABLA FINAL
            print('Iniciando carga de datos de tabla temporal a tabla final')
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Iniciando carga de datos de tabla temporal a tabla final ' + tabla_final)
            query_insert = modify_temporary_table(cfg_, tmp_table_storage, tabla_final)
            print('Sentencia insert de la tabla TMP a tabla FINAL: ', query_insert)
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Sentencia insert de la tabla TMP a tabla FINAL: ' + query_insert)
            
            if query_insert is None or query_insert == "":
                raise ExceptionInternal(codigo=403, mensaje=f'Error al generar insert de la tabla TMP a tabla final :: {query_insert}')
            
            #SE EJECUTA QUERY DEL INSERT GENERADO
            cursor.execute(query_insert)
            session.commit()
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Query Insert ejecutado con exito: ')
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Datos cargados con exito a tabla final: ' + tabla_final)
            print('Datos cargados con exito a tabla final ', tabla_final)
            
            print('Eliminando tabla temporal..')
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Iniciando eliminacion de tabla temporal ' + tmp_table_storage)
            cursor.execute(f"DROP TABLE IF EXISTS {tmp_table_storage}")
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Query a ejecutar: ' + f'DROP TABLE IF EXISTS {tmp_table_storage}')
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Tabla temporal eliminada con exito')
            print('Tabla temporal eliminada con exito')
            
            print('Eliminando archivo .CSV generado..')
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Iniciando eliminacion de archivo .CSV generado')
            
            delete_file_csv(r_dat)
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + f' :: Archivo de la ruta {r_dat} eliminado exitosamente')
            
            return JSONResponse(status_code=200, content={"code": 0 , "message": "Exito"})

    except ExceptionInternal as e:
        update_log_file(archivo, 'UPLOAD', False, e.mensaje)
        delete_table_temporary(tmp_table_storage)        
        delete_file_csv(r_dat)
        print('Error: ' , e.mensaje)
        logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Error - ' + e.mensaje)
        return JSONResponse(status_code=e.codigo, content={"code": 1, "message": f"{e.mensaje}"})
        
    except OSError as err:
        update_log_file(archivo, 'UPLOAD', False, f'{err}')
        delete_table_temporary(tmp_table_storage)        
        delete_file_csv(r_dat)
        
        logging.exception(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Error en load_file_table - No se puedo eliminar los archivos: ' + str(err))
        return JSONResponse(status_code=500, content={"code": 1, "message": f"{err}"})
    
    except Exception as er:
        update_log_file(archivo, 'UPLOAD', False, f'{er}')
        delete_table_temporary(tmp_table_storage)        
        delete_file_csv(r_dat)         
           
        logging.exception(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Error en load_file_table - Warning por exception: ' + str(er))
        return JSONResponse(status_code=500, content={"code": 1, "message": f"{er}"})


# ===========================================#
#    F U N C I O N  -  P R I N C I P A L 	 #
# ========================================== #

def upload_file_etl(file_name, p_date):
    
    f_date = horario_local.strftime(YYYYMMDD)
    print('Archivo a cargar: ', file_name)
    print('Fecha: ', p_date) 
    
    # -----------------------------------------------------------------------#
    #                 I N I C I O   D E L   P R O G R A M A                  #
    # -----------------------------------------------------------------------#
    
    try:
        #LECTURA DE ARCHIVO DE CONFIGUACION PARA LA TABLA
        with open(config.PATH_PARAMETERS_JSON, 'r') as file:
            parameters_data = json.load(file)
            config_validation = parameters_data['configuration_validation']
        
        if file_name.upper() not in config_validation:
            raise ExceptionInternal(codigo=404, mensaje='No existe datos configurados para realizar la carga del archivo')

        cfg_file = config_validation[file_name]
        path_base = cfg_file['path']
        path_file = cfg_file['path_file']
        header_file = cfg_file['header_file']
        file_extension = cfg_file['extension']
        delimiter_file =  cfg_file['delimiter']        
        name_patter = cfg_file['file_pattern_name']
        table_details = cfg_file['table_details_load']
        config_campo_fecha = cfg_file['config_field_date']
        reprocess_date_d = cfg_file['reprocess_date_detail']
        
        fecha_archivo = get_file_date_format(name_patter, p_date)
        
        #-------------------------------------------------------------------------#
        #                             R   U   T  A   S                            #
        #-------------------------------------------------------------------------#

        print('\n\t*** DATOS CONFIGURADOS DEL ARCHIVO ::', file_name, ' ***\n')
        
        name_log = 'LOAD' + chr(95) + file_name + chr(95) + f_date + '.log'
        name_file = file_name + chr(95) + fecha_archivo + chr(46) + file_extension
        log_path = path_base + 'log/'
        dat_path = path_base + 'dat/'
        file_path_load = path_file + 'INPUT/'
        backup_path = path_file + 'BACKUP/' + name_file
        path_file_final = file_path_load + name_file 
        print('Ruta base: ', path_base, '\nRuta del archivo a cargar: ', file_path_load, '\nNombre del archivo log: ', name_log,  '\nRuta Log: ',log_path, '\nRuta Dat: ', dat_path, '\nRuta Backup: ', backup_path)
        
        logging.basicConfig(filename=log_path+name_log, level=logging.INFO, filemode='w')
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' ** INICIO DEL PROGRAMA **')
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Archivo a cargar: ' + name_file)
        
        #-------------------------------------------------------------------------#
        #                 I N I C I O    D E L    P R O G R A M A                 #
        #-------------------------------------------------------------------------# 
        
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Depurando archivos .log')
        
        print('Depurando archivos .log  Y .csv antiguos')
        remove_file(log_path, 7, '.log')
        remove_file(dat_path, 7, '.csv')
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Depurando archivos respaldados -> .xlsx , .xls, .csv antiguos')
        print('Depurando archivos respaldados -> .xlsx , .xls, .csv antiguos')
        
        #OBTENER EL VALOR DEL CAMPO FECHA
        v_fecha_tmp = generate_date_field(p_date, reprocess_date_d)
        print('Campo fecha: ',v_fecha_tmp )
        
        #VALIDAR SI EXISTE MAS DE UN VALOR O CAMPO FECHA
        fecha_tmp = get_value_field_date(v_fecha_tmp, config_campo_fecha, reprocess_date_d)
        print('fecha_tmp: ',fecha_tmp )
        
        #-------------------------------------------------------------#
        #       D A T O S   P A R A    N O T I F I C A C I O N        #
        #-------------------------------------------------------------# 
        
        print('\n ** Validando si existe archivo en la ruta **  :: ', path_file_final)
        query_path_file = validate_file_path(file_path_load, path_file_final)
        print('Respuesta: ', query_path_file)
        file_without_extension, file_extension = get_file_name_extension(path_file_final)
        
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Archivo sin extension: ' + file_without_extension)
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Extension del archivo: ' + file_extension)
        
        #SI NO EXISTE EL ARCHIVO
        if not query_path_file:
            raise ExceptionInternal(codigo=404, mensaje=f'No existe archivo {name_file} en la ruta {file_path_load}')
        
        #EXISTE ARCHIVO
        print('Se comprueba que archivo si existe en la ruta\n') 
        #-------------------------------------------------------------------------#
        #         I N I C I A N D O  C A R G A   D E   A R C H I V O              #
        #-------------------------------------------------------------------------#  
        
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Se comprueba que archivo si existe en la ruta')
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Iniciando Carga de Archivo')
                
        response_load = load_file_table(v_fecha_tmp, 
                                   path_file_final, 
                                   file_extension, 
                                   file_name, 
                                   dat_path, 
                                   table_details, 
                                   reprocess_date_d, 
                                   cfg_file, 
                                   config_campo_fecha,
                                   header_file,
                                   delimiter_file)
        
        content = response_load.body
        json_string = content.decode('utf-8')
        data = json.loads(json_string)
        print('message: ', data["message"], ' - ', 'code: ', data["code"])
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Respuesta de carga de archivo [code] : ' + str(data["code"]))
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Respuesta de carga de archivo [message] : ' + str(data["message"]))
        
        if int(data["code"]) > 0:
            raise ExceptionInternal(codigo=500, mensaje=str(data["message"]))
        
        #------------------------------------------------------------------------------#
        #  M O V I E N D O   A R C H I V O   A   C A R P E T A   D E   R E S P A L D O #
        #------------------------------------------------------------------------------#
        
        move_backup_file(path_file_final, backup_path) 
        
        #------------------------------------------------------------------------------#
        #        A C T U A L I Z A R   B I T A C O R A   DE    A R C H I V O           #
        #------------------------------------------------------------------------------#
        
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Inicia actualización bitacora de carga de Archivo')
        update_log_file(file_name, 'UPLOAD', True, 'Cargado con éxito')
            
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Fin Carga de Archivo')
        #-------------------------------------------------------------------------#
        #               F I N    C A R G A   D E   A R C H I V O                  #
        #-------------------------------------------------------------------------#
        
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' ** FIN DEL PROGRAMA **')                  
        
        #-------------------------------------------------------------------------#
        #                    F I N    D E L   P R O G R A M A                     #
        #-------------------------------------------------------------------------#
            
        print("Archivo cargado con exito")
        return JSONResponse(status_code=200, content={"code": 0, "message": "Archivo cargado con éxito"})
    
    except ExceptionInternal as e:
        update_log_file(file_name, 'UPLOAD', False, e.mensaje)
        print("Error upload_file_etl-ExceptionInternal: ", e.mensaje)
        logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: ' + json.dumps({"code": e.codigo, "message": e.mensaje}))
        return JSONResponse(status_code=e.codigo, content={"code": 1, "message": e.mensaje})
    
    except Exception as ex:
        update_log_file(file_name, 'UPLOAD', False, f'{ex}')
        print('Error en upload_file_etl: ', ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"{ex}"})