
import json
from fastapi.responses import JSONResponse
from app.config.config import Config

config = Config()

# ================================================ #
#  Q U E R Y  S T A T U S  F I L T E R   F I L E   #
# ================================================ #

def query_status_file_filter_etl(key: str, phase: str):
    try:
        with open(config.PATH_LOG_INFORMATION, 'r') as file:
            data = json.load(file)
        
        if key in data["log_information"]:
            if phase == "VALIDATE":
                data_filter = data["log_information"][key]["validation_phase"]
                rsp = {
                    "code": 0, 
                    "message": "Exito",
                    "content": data_filter
                }
                return JSONResponse(status_code=200, content=rsp)
            else:
                data_filter = data["log_information"][key]["loading_phase"]
                rsp = {
                    "code": 0, 
                    "message": "Exito",
                    "content": data_filter
                }
                return JSONResponse(status_code=200, content=rsp)
                
        return JSONResponse(status_code=404, content={"code": 1, "message": f"No existe registros del archivo {key}"})
        
    except Exception as ex:
        print('Error en query_status_file_etl: ', ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error - {ex}"})


# ======================================================== #
#  Q U E R Y  S T A T U S  F I L T E R   N A M E  F I L E  #
# ======================================================== #

def query_status_file_filter_name_file_etl(key: str):
    try:
        with open(config.PATH_LOG_INFORMATION, 'r') as file:
            data = json.load(file)
        
        if key in data["log_information"]:
            key_data = data["log_information"][key]
            rsp = {
                "code": 0, 
                "message": "Exito",
                "content": key_data
            }
            return JSONResponse(status_code=200, content=rsp)
                
        return JSONResponse(status_code=404, content={"code": 1, "message": f"No existe registros del archivo {key}"})
        
    except Exception as ex:
        print('Error en query_status_file_filter_name_file_etl: ', ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error - {ex}"})
    
# ========================================= #
#  Q U E R Y  S T A T U S  A L L   F I L E  #
# ========================================= #

def query_status_file_all_etl():
    try:
        with open(config.PATH_LOG_INFORMATION, 'r') as file:
            data = json.load(file)
            all_data = data["log_information"]
            
        rsp = {
                "code": 0, 
                "message": "Exito",
                "content": all_data
            }
        
        return JSONResponse(status_code=200, content=rsp)
    
    except Exception as ex:
        print('Error en query_status_file_all_etl: ', ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error - {ex}"})