#==================================#
#        L I B R E R I A S         #
#==================================#
import re
import pytz
import logging
from datetime import datetime
import json
from fastapi.responses import JSONResponse
from app.config.config import Config
from app.middlewares.excepciones import ExceptionInternal
from app.util.functions import (generate_dict_error_file, 
                                get_dataframe,
                                get_file_column_information,
                                get_file_date_format,
                                get_file_name_extension,
                                has_required_value,
                                remove_file,
                                update_log_file,
                                validate_file_headers,
                                validate_file_path)

ANIO_MES_DIA_HHMMSS = "%Y-%m-%d %H:%M:%S"
YYYYMMDD='%Y%m%d'
CONTENT_NOT_ALLOWED = 'Contenido no permitido'
horario_local = datetime.now(pytz.timezone('America/Guayaquil'))

config = Config()


# ------------------------------------------ #
# F U N C T I O N S   R U L E S   B A S I C  #
# ------------------------------------------ #

def get_key_rules_validations(column_name, rules_validations):
    for dic in rules_validations:
        if dic['column_validate'] == column_name:
            upper = dic['upper']
            length = dic['length']
            basic_rules = dic['rules']['basic_rules']
            specific_rules = dic['rules']['specific_rules']
            return basic_rules, specific_rules, upper, length
    return [], [], None, None


#VALIDAR EL TAMANIO DE LA COLUMNA
def validate_size(df, column_name, max_size):
    errors = []
    for index, value in df[column_name].items():
        if len(str(value).strip()) > max_size:
            logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: '+ str(generate_dict_error_file(int(index) + 2, value, f'Contenido excede del tamaño permitido ({max_size})')))
            errors.append(generate_dict_error_file(int(index) + 2, value, f'Contenido excede del tamaño permitido ({max_size})'))
    return errors

#VALIDAR SI E SUN TEXTO
def validate_string(df, column_name):
    errors = []
    for index, value in df[column_name].items():
        if not isinstance(value, str) and (str(value).isdigit()):
            logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: '+ str(generate_dict_error_file(int(index) + 2, value, 'Contenido no es texto')))
            errors.append(generate_dict_error_file(int(index) + 2, value, 'Contenido no es texto'))
    return errors

#VALIDAR SI ES NUMERICO (ENTERO O DECIMAL)
def validate_numeric(df, column_name):
    errors = []
    for index, value in df[column_name].items():
        try:
            int_value = int(value)
            if str(int_value) == str(value):
                continue
        except ValueError:
            pass
        try:
            float_value = float(value)
            if str(float_value) == str(value):
                continue 
        except ValueError:
            logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: ' + str(generate_dict_error_file(int(index) + 2, value, 'Contenido no es numérico')))
            errors.append(generate_dict_error_file(int(index) + 2, value, 'Contenido no es numérico'))
    return errors


#VALIDAR SI ES FECHA
def validate_date(df, column_name):
    errors = []
    format_date = r'^\d{1,2}[-/]\d{1,2}[-/]\d{4} \d{1,2}:\d{1,2}:\d{1,2}$|^\d{4}[-/]\d{1,2}[-/]\d{1,2} \d{1,2}:\d{1,2}:\d{1,2}$'
    for index, value in df[column_name].items():
        if re.match(format_date, str(value)):
            continue
        else:
            logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: '+ str(generate_dict_error_file(int(index) + 2, value, 'Contenido su formato de fecha es incorrecto')))
            errors.append(generate_dict_error_file(int(index) + 2, value, 'Contenido su formato de fecha es incorrecto'))
    return errors

#VALIDAR SI ES ALFANUMERICO
def validate_alphanumeric(df, column_name):
    errors = []
    for index, value in df[column_name].items():
        if not str(value).isalnum():
            logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: '+ str(generate_dict_error_file(int(index) + 2, value, 'Contenido su valor no es alfanumérico')))
            errors.append(generate_dict_error_file(int(index) + 2, value, 'Contenido su valor no es alfanumérico'))
    return errors

#VALIDAR SI ES NEGATIVO
def validate_negative(value):
    try:
        num = float(value)
        return num < 0
    except ValueError:
        return False

#VALIDAR NEGATIVOS
def validate_numeric_negative(df, column_name):
    errors = []
    for index, value in df[column_name].items():
        if not validate_negative(value):
            logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: '+ str(generate_dict_error_file(int(index) + 2, value, 'Contenido no es un número negativo válido')))
            errors.append(generate_dict_error_file(int(index) + 2, value, 'Contenido no es un número negativo válido'))
    return errors


#VALIDAR DUPLICADOS
def validate_duplicates(df, column_name):
    duplicates = df[df.duplicated(subset=[column_name], keep=False)]
    errors = []
    for index, value in duplicates.iterrows():
        logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: '+ str(generate_dict_error_file(int(index) + 2, value, 'Contenido esta duplicado con el anterior')))
        error = generate_dict_error_file(int(index) + 2, value, 'Contenido esta duplicado con el anterior')
        errors.append(error)
    return errors


# ------------------------------------------------ #
# F U N C T I O N S   R U L E S   S P E C I F I C  #
# ------------------------------------------------ #

def has_required_value(df, column_name, allowed_values, convert_upper):
    errores = []
    for index, value in df[column_name].items():
        value_strip = str(value).strip()
        n_value = value_strip.upper() if convert_upper else value
        if n_value not in allowed_values:
            logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: '+ str(generate_dict_error_file(int(index) + 2, value, CONTENT_NOT_ALLOWED)))
            errores.append(generate_dict_error_file(int(index) + 2, value, CONTENT_NOT_ALLOWED))
    return errores


def start_required_value(df, column_name, allowed_prefixes, convert_upper):
    errors = []
    for index, value in df[column_name].items():
        value_strip = str(value).strip()
        n_value = value_strip.upper() if convert_upper else value
        if not any(n_value.startswith(prefix) for prefix in allowed_prefixes):
            logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: '+ str(generate_dict_error_file(int(index) + 2, value, CONTENT_NOT_ALLOWED)))
            errors.append(generate_dict_error_file(int(index) + 2, value, CONTENT_NOT_ALLOWED))
    return errors


def not_contain_value2(df, column_name, invalid_chars):
    errors = []
    for index, value in df[column_name].items():
        if df[column_name].str.contains('|'.join(invalid_chars)).any():
            logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: '+ str(generate_dict_error_file(int(index) + 2, value, CONTENT_NOT_ALLOWED)))
            errors.append(generate_dict_error_file(int(index) + 2, value, CONTENT_NOT_ALLOWED))
    return errors

def not_contain_value(df, column_name, invalid_chars):
    errors = []
    for index, value in df[column_name].items():
        if any(char in str(value) for char in invalid_chars):
            logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: '+ str(generate_dict_error_file(int(index) + 2, value, CONTENT_NOT_ALLOWED)))
            errors.append(generate_dict_error_file(int(index) + 2, value, CONTENT_NOT_ALLOWED))
    return errors


# ------------------------- #
#   R U L E S   B A S I C   #
# ------------------------- #

def apply_basic_rules(df, column_name, basic_rules, size):
    try:
        print('size: ', size)
        print('column_name: ', column_name)
        print('basic_rules: ', basic_rules)
        errors_basic = []
        for rule in basic_rules:
            #REGLAS BASICAS
            if rule == 'VAL_TAM':
                errors_basic += validate_size(df, column_name, size)
            elif rule == 'VAL_TXT':
                errors_basic += validate_string(df, column_name)
            elif rule == 'VAL_NUM':
                errors_basic += validate_numeric(df, column_name)
            elif rule == 'VAL_FEC':
                errors_basic += validate_date(df, column_name)
            elif rule == 'VAL_ALF':
                errors_basic += validate_alphanumeric(df, column_name)
            elif rule == 'VAL_NEG':
                errors_basic += validate_numeric_negative(df, column_name)
            elif rule == 'VAL_DUP':
                errors_basic += validate_duplicates(df, column_name)
                
        return errors_basic
    except Exception as ex:
        print('Error en apply_basic_rules: ', ex)


# -------------------------------- #
#   R U L E S    S P E C I F I C   #
# -------------------------------- #

def apply_specific_rules(df, column_name, specific_rules, upper):
    try:
        print('column_name: ', column_name)
        print('specific_rules: ', specific_rules)
        print('upper: ', upper)
        errors = []
        for rule in specific_rules:
            if rule['specific_rule_type'] == "required_value" and rule['rule_property']:
                allowed_values = rule['rule_property']
                errors += has_required_value(df, column_name, allowed_values, upper)
            elif rule['specific_rule_type'] == "start_value" and rule['rule_property']:
                allowed_values = rule['rule_property']
                errors += start_required_value(df, column_name, allowed_values, upper)
            elif rule['specific_rule_type'] == "invalid_value" and rule['rule_property']:
                invalid_chars = rule['rule_property']
                errors += not_contain_value(df, column_name, invalid_chars)
        
        return errors
    except Exception as ex:
        print('Error en apply_specific_rules: ', ex)


# ---------------------------- #
#      V A L I D A T O R       #
# ---------------------------- #

def validate_file(df, rules_validations):
    try:
        content = []
        
        for column_name in df.columns:
            basic_rules, specific_rules, upper, size = get_key_rules_validations(column_name, rules_validations)

            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Columna a validar: ' + column_name)
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Reglas Basicas: ' + str(basic_rules))
            logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Reglas Especificas: ' + str(specific_rules))

            column_content = {"name_column": column_name, "detail_error": []}
            if basic_rules:
                basic_errors = apply_basic_rules(df, column_name, basic_rules, size)
                if basic_errors:
                    for error in basic_errors:
                        column_content["detail_error"].append({"fila": error["fila"], 
                                                               "registro": error["registro"], "error": error["error"]})

            if specific_rules:
                specific_errors = apply_specific_rules(df, column_name, specific_rules, upper)
                if specific_errors:
                    for error in specific_errors:
                        column_content["detail_error"].append({"fila": error["fila"],
                                                               "registro": error["registro"], "error": error["error"]})

            if column_content["detail_error"]:  
                content.append(column_content)
            
        return content
    except Exception as ex:
        print('Error en validate_file: ', ex)
        return None


# ========================================== #
#    F U N C I O N  -  P R I N C I P A L 	 #
# ========================================== #

def validator_file_etl(file_name, p_date):
    
    #-------------------------------------------------------------------------#
    #                 I N I C I O   D E L   P R O G R A M A                   #
    #-------------------------------------------------------------------------#
    # CONSULTA CONFIGURACION #
    try:
        with open(config.PATH_PARAMETERS_JSON, 'r') as file:
            parameters_data = json.load(file)
            config_validation = parameters_data['configuration_validation']
        
        if file_name.upper() not in config_validation:
            raise ExceptionInternal(codigo=404, mensaje=f'No existe configuración para el archivo {file_name}')
        
        config_file = config_validation[file_name]
        path_base = config_file['path']
        delimiter = config_file['delimiter']
        header_file = config_file['header_file']
        file_path_load = config_file['path_file']
        file_extension = config_file['extension']
        name_patter = config_file['file_pattern_name']
        columns_name_list = config_file['columns_name']
        rules_validations = config_file['rules_validations']
        
        print('\n\t*** DATOS CONFIGURADOS DEL ARCHIVO ::', file_name, ' ***\n')
        print(f'Ruta archivo: {file_path_load} \nExtension archivo: {file_extension} \nPatron de archivo: {name_patter} \nLista de Columnas: {columns_name_list}')
              
        #OBTENER LA FECHA DEL ARCHIVO DESDE LA FECHA
        file_date = get_file_date_format(name_patter, p_date)
        
        #---------------------------------#
        #            R U T A S            #
        #---------------------------------#
        print('\n\t*** RUTAS ***\n')
        d_date = horario_local.strftime(YYYYMMDD)
        file_path = file_path_load + 'INPUT/'
        log_path = path_base + 'log/'
        name_log = file_name + chr(95) + d_date + '.log'
        name_file_load = file_name + chr(95) + file_date + chr(46) + file_extension
        d_archivo = file_path + name_file_load
        print('name_file_load: ', name_file_load,  "\nRuta Log: ",log_path, "\nRuta de Base:", d_archivo)
        
        logging.basicConfig(filename=log_path+name_log, level= logging.INFO, filemode='w')
        logging.info (horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' ** INICIO DEL PROGRAMA **')
        logging.info (horario_local.strftime(ANIO_MES_DIA_HHMMSS) + '\n*** DATOS CONFIGURADOS DEL ARCHIVO :: ' + file_name + ' ***\n')
        
        logging.info (horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Archivo a validar: '+ name_file_load)
        print('Archivo a validar: ', name_file_load)
        
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Eliminando logs antiguos')
        print('\nEliminando logs antiguos...')
        remove_file(path_base + 'log/', 7, '.log')
        
        # ------------------------------------------------- #
        #   O B T I E N E    D A T O S    A R C H I V O     #
        # ------------------------------------------------- #
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Consultando si existe ruta')
        print('\nConsultando si el archivo existe en la ruta')
        
        query_path_file = validate_file_path(file_path, d_archivo)
        
        #SI NO EXISTE EL ARCHIVO
        if not query_path_file:
            raise ExceptionInternal(codigo=404, mensaje=f'No existe archivo {d_archivo} en la ruta: {file_path}')
        
        #SI EXISTE
        print('Archivo '+ name_file_load +' si existe en la ruta\n')
        file_without_extension , file_extension = get_file_name_extension(d_archivo)
        
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Nombre de archivo: ' + file_without_extension)
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Extension del archivo: ' + file_extension)
        print('Nombre de archivo: ', file_without_extension , '\nExtension del archivo:', file_extension)
        
        #-------------------------------------#
        #    D A T A F R A M E    B A S E	  #
        #-------------------------------------#
        print('\n*** GENERANDO DATAFRAME ***')
        df_master_file = get_dataframe(d_archivo, file_extension, header_file, delimiter)
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Generando DataFrame..\n' + str(df_master_file))
        print(df_master_file)
        
        #--------------------------------------------------------------------------#
        #              V A L I D A C I O N    D E    A R C H I V O                 #
        #--------------------------------------------------------------------------#
        print('\n*** VALIDACION DE ARCHIVO ***\n')
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Obteniendo informacion del archivo..')
        
        tot_rows_file, tot_col_file, list_file_columns = get_file_column_information(df_master_file)
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Total de filas: '+ str(tot_rows_file) +'\n\t\t\t Total de Columnas: '+ str(tot_col_file) +'\n\t\t\t Nombre de Columnas: '+ str(list_file_columns))
 
        print('Validando la Cabecera del archivo ',name_file_load,' [COLUMNAS]')
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Validando Nombre Columnas archivo Vs Nombre Columnas Configuracion.')
        match_headers = validate_file_headers(list_file_columns, columns_name_list)
        
        if not ((tot_col_file == len(columns_name_list)) and match_headers):
            raise ExceptionInternal(codigo=404, mensaje='No coincide el total de columnas o el nombre de la columna del archivo')
        
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Iniciando Validacion del archivo')
        
        # INICIAR VALIDACION DE ARCHIVO
        print('Validando Datos del archivo')
        
        validate_file_1 = validate_file(df_master_file, rules_validations)
        rsp = {
            "code": 0, 
            "message": "Archivo validado",
            "content": validate_file_1
            }
        
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Finalizacion de Validacion del archivo')
        logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: Resultado: ' + str(rsp))
        # FINALIZAR VALIDACION DE ARCHIVO
        if not validate_file_1:
            update_log_file(file_name, 'VALIDATE', True, 'Validado con éxito')
        else: 
            update_log_file(file_name, 'VALIDATE', True, 'Discrepancias en columnas del archivo. Verificar detalle en la tabla')
            
        return JSONResponse(status_code=200, content=rsp)
        
    except ExceptionInternal as e:
        update_log_file(file_name, 'VALIDATE', True, str(e.mensaje))
        print(json.dumps({"code": e.codigo, "message": e.mensaje}))
        logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: ' + json.dumps({"codigo": e.codigo, "message": e.mensaje}))
        return JSONResponse(status_code=e.codigo, content={"code": 1, "message": f"{e.mensaje}"})
    
    except Exception as ex:
        print('Error en validator_file_etl: ', ex)
        update_log_file(file_name, 'VALIDATE', True, f'{str(ex)}')
        logging.exception(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + f' :: Error en validator_file_etl: {ex}')
        return JSONResponse(status_code=500, content={"code": 1, "message": f"{str(ex)}"})
    