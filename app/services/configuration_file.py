import os
import json
from pathlib import Path
import pandas as pd
from typing import Optional, List
from fastapi import HTTPException
from fastapi.responses import JSONResponse
from app.config.config import Config
from app.util.functions import connection_server_ubuntu

config = Config()

# ===================================================== #
# Q U E R Y   C O N F I G U R A T I O N   B Y   N A M E #
# ===================================================== #

def query_configuration_by_name_etl(key):
    try:
        with open(config.PATH_PARAMETERS_JSON, 'r') as file:
            data = json.load(file)
        if key in data["configuration_validation"]:
            rsp = {
                "code": 0, 
                "message": "Exito",
                "content": data["configuration_validation"][key]
            }
            return JSONResponse(status_code=200, content=rsp)
        
        return JSONResponse(status_code=404, content={"code": 1, "message": f"El archivo {key} no existe"})
                
    except Exception as ex:
        print("Error en query_configuration_by_name_etl: ", ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error - {ex}"})


# ========================================= #
# Q U E R Y   C O L U M N S   B Y   N A M E #
# ========================================= #

def query_columns_configured_by_name_etl(key):
    try:
        with open(config.PATH_PARAMETERS_JSON, 'r') as file: 
            data = json.load(file)
        #CONSULTAR INFORMACION
        if key in data["configuration_validation"]:
            columns = data["configuration_validation"][key]["columns_name"]
            if columns:
                rsp = {
                    "code": 0, 
                    "message": "Exito",
                    "content": columns
                }
                return JSONResponse(status_code=200, content=rsp)
            
            return JSONResponse(status_code=404, content={"code": 1, "message": "No se encontraron columnas configuradas."})
        return JSONResponse(status_code=404, content={"code": 1, "message": f"El archivo {key} no existe"})
    except Exception as ex:
        print("Error en query_columns_configured_by_name_etl: ", ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error - {ex}"})
    

# ===================================== #
# Q U E R Y   R U L E S   B Y   N A M E #
# ===================================== #
    
def query_rules_configured_by_name_etl(key):
    try:
        with open(config.PATH_PARAMETERS_JSON, 'r') as file: 
            data = json.load(file)
        #CONSULTAR INFORMACION
        if key in data["configuration_validation"]:
            rules_validations = data["configuration_validation"][key]["rules_validations"]
            if rules_validations:
                rsp = {
                    "code": 0, 
                    "message": "Exito",
                    "content": rules_validations
                }
                return JSONResponse(status_code=200, content=rsp)
            
            return JSONResponse(status_code=404, content={"code": 1, "message": "No se encontraron reglas configuradas."})   
        return JSONResponse(status_code=404, content={"code": 1, "message": f"El archivo {key} no existe"})
                
    except Exception as ex:
        print("Error en query_rules_basic_configured_by_name_etl: ", ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error - {ex}"})


# ============================================================================= #
# Q U E R Y   R U L E S   S P E C I F I C / B A S I C   B Y   N A M E   F I L E #
# ============================================================================= #

def query_rules_filter_by_name_etl(key, type_rule):
    try:
        with open(config.PATH_PARAMETERS_JSON, 'r') as file: 
            data = json.load(file)
        #CONSULTAR INFORMACION
        if key in data["configuration_validation"]:
            rules_validations = data["configuration_validation"][key]["rules_validations"]
            if rules_validations:
                if type_rule == 'B':
                    columns_validacion = [{"column_name": regla["column_validate"], 
                                        "basic_rules": regla["rules"]["basic_rules"]} for regla in rules_validations]
                else:
                    columns_validacion = [{"column_name": regla["column_validate"], 
                                            "specific_rules": regla["rules"]["specific_rules"]} for regla in rules_validations]
                rsp = {
                    "code": 0, 
                    "message": "Exito",
                    "content": columns_validacion
                }
                msg_rules = 'basicas' if type_rule == 'B' else 'especificas'
                return JSONResponse(status_code=200, content=rsp)
            return JSONResponse(status_code=404, content={"code": 1, "message": f"No se encontraron reglas {msg_rules} configuradas."})
        return JSONResponse(status_code=404, content={"code": 1, "message": f"El archivo {key} no existe"})        
            
    except Exception as ex:
        print("Error en query_rules_specific_configured_by_name_etl: ", ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error - {ex}"})
    

# ================================================================================= #
# Q U E R Y   R U L E S   S P E C I F I C / B A S I C   B Y   N A M E   C O L U M N #
# ================================================================================= #

def rules_filter_by_name_column_etl(key, key_name_column, type_rule):
    try:
        with open(config.PATH_PARAMETERS_JSON, 'r') as file:
            data = json.load(file)

        if key not in data.get("configuration_validation", {}):
            return JSONResponse(status_code=404, content={"code": 1, "message": f"El archivo {key} no existe"})

        rules_validations = data["configuration_validation"][key].get("rules_validations", [])
        found = False

        for column in rules_validations:
            if column.get("column_validate") == key_name_column:
                found = True
                msg_rules = 'basicas' if type_rule == 'B' else 'especificas'
                rules = column["rules"]["basic_rules"] if type_rule == 'B' else column["rules"]["specific_rules"]
                return JSONResponse(status_code=200, content={"code": 0, "message": "Exito", "content": rules})

        if not found:
            return JSONResponse(status_code=404, content={"code": 1, "message": f"La columna {key_name_column} no existe en el archivo {key}"})

        return JSONResponse(status_code=404, content={"code": 1, "message": f"No se encontraron reglas {msg_rules} configuradas."})

    except Exception as ex:
        print("Error en rules_specific_configured_by_name_column_etl:", ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error - {ex}"})




# ======================================================== #
# C R E A T E   T E M P L A T E   C O N F I G U R A T I O N #
# ======================================================== #

def add_new_section(json_data, section_name):
    try:
        default_header_file = False
        new_configuration = {
            section_name: {
                "path": config.PATH_BASE,
                "path_file": config.PATH_UPLOAD_FOLDER,
                "file_pattern_name": "",
                "extension": "",
                "header_file": default_header_file,
                "delimiter": "|",
                "table_details_load": {},
                "reprocess_date_detail": {},
                "config_field_date": {},
                "columns_name": [],
                "rules_validations": []
                }
        }
        
        json_data["configuration_validation"].update(new_configuration)
        return 0
    except Exception as ex:
        print('Error en add_new_section: ', ex)
        return 1
        
        
def configuration_file_etl(name_configuration: str):
    try:
        with open(config.PATH_PARAMETERS_JSON, 'r') as json_file:
            existing_json = json.load(json_file)
            
        rsp = add_new_section(existing_json, name_configuration)
        if rsp > 0:
            return JSONResponse(status_code=500, content={"code": 1, "message": "No se pudo agregar la configuracion"})
            
        json_string = json.dumps(existing_json, indent=2)
        
        with open(config.PATH_PARAMETERS_JSON, 'w') as json_file:
            json_file.write(json_string)
            
        return JSONResponse(status_code=200, content={"code": 0, "message": "configuracion realizada"})
    
    except Exception as ex:
        print('Error en configuration_file_etl: ', ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error - {ex}"})


# ======================================= #
# D E L E T E   C O N F I G U R A T I O N #
# ======================================= #

def remove_element(json_data, key):
    if key in json_data:
        del json_data[key]
        print(f"Elemento con clave '{key}' eliminado.")
        return 0
    else:
        print(f"Elemento con clave '{key}' no encontrado.")
        return 1
   
        
def remove_configuration_file_etl(section_name):
    try:
        with open(config.PATH_PARAMETERS_JSON, 'r') as json_file:
            existing_json = json.load(json_file)
        
        rsp = remove_element(existing_json['configuration_validation'], section_name)
        if rsp > 0:
            return JSONResponse(status_code=400, content={"code": 1, "message": "No exiSte el archivo a eliminar"})
            
        with open(config.PATH_PARAMETERS_JSON, 'w') as json_file:
            json.dump(existing_json, json_file, indent=2)
        
        return JSONResponse(status_code=200, content={"code": 0, "message": "Registro eliminado"})
            
    except Exception as ex:
        print('Error en remove_configuration_file: ', ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error - {ex}"})
    
# ======================================= #
#          A D D    C O L U M N S         #
# ======================================= #

def get_column_names(extension_archivo, ruta_archivo):
    if extension_archivo == 'xlsx':
        df = pd.read_excel(ruta_archivo, engine="openpyxl", dtype=str)
    elif extension_archivo == 'xls':
        df = pd.read_excel(ruta_archivo, dtype=str)
    else:
        df = pd.read_csv(ruta_archivo, delimiter=";", dtype=str)
    #  QUITAR LOS VALORES NaN o ESPACIOS  #
    df.fillna('', inplace = True)
    df.replace(' ', '', inplace = True)
    return list(df.columns)


def add_columns_configuration_file_etl(lista_columnas: list):
    try:
        pass
    except Exception as ex:
        print('Error en add_columns_configuration_file: ', ex)


# ======================================= #
#        Q U E R Y    C O L U M N S       #
# ======================================= #

def get_dataframe(ruta_archivo, extension_archivo):
    try:
        if extension_archivo == 'xlsx':
            df = pd.read_excel(ruta_archivo, engine="openpyxl", dtype=str)
        elif extension_archivo == 'xls':
            df = pd.read_excel(ruta_archivo, dtype=str)
        else:
            df = pd.read_csv(ruta_archivo, delimiter=";", dtype=str)
        #  QUITAR LOS VALORES NaN o ESPACIOS  #
        df.fillna('', inplace = True)
        df.replace(' ', '', inplace = True)
        return df
    except Exception as ex:
        print('Error en get_dataframe: ', ex)
        


def query_configuration_file_column_etl(name_file, ruta_archivo):
    try:
        path_file_final = os.path.join(ruta_archivo, name_file)
        file_extension = Path(name_file).suffix.lstrip(".")
        #GENERAR DATAFRAME
        df = get_dataframe(path_file_final, file_extension)
        
        return JSONResponse(status_code=200, content={"code": 0, "message": f"Se obtuvo informacion del archivo [{name_file}] exitosamente", "list_columns": list(df.columns)})
        
    except Exception as ex:
        print('Error en query_configuration_file_etl: ', ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error - {ex}"})
    
# =============================================================================== #
# A D D  C O N T E N T   R U L E S   S P E C I F I C    B Y   N A M E   R U L E   #
# =============================================================================== #

def save_data_to_json(data):
    with open(config.PATH_PARAMETERS_JSON, 'w') as file:
        json.dump(data, file)

def add_content_rules_to_specific_rule(specific_rule, elements):
    for new_element in elements:
        element_found = False
        for i, existing_elem in enumerate(specific_rule):
            print('i: ', i)
            print('existing_elem: ', existing_elem)
            if existing_elem.lower() == new_element.lower():
                specific_rule[i] = new_element
                element_found = True
                break
    
        if not element_found:
            specific_rule.append(new_element)
    
        

def add_content_rules_specific_by_name_rule_etl(name_file, name_column, key_rule, elements: List[str]):
    try:        
        error_messages = []
        if not elements:
            return JSONResponse(status_code=400, content={"code": 1, "message": "La lista de elementos no puede estar vacia"})

        with open(config.PATH_PARAMETERS_JSON, 'r') as file:
            data = json.load(file)

        if name_file not in data["configuration_validation"]:
            return JSONResponse(status_code=404, content={"code": 1, "message": f"No se encuentra información del archivo {name_file}"})

        for rule in data["configuration_validation"][name_file]["rules_validations"]:
            if rule["column_validate"] == name_column:
                for s_rule in rule["rules"]["specific_rules"]:
                    if s_rule["specific_rule_type"] == key_rule:
                        rule_property = s_rule["rule_property"]
                        add_content_rules_to_specific_rule(rule_property, elements)
                        save_data_to_json(data)
                        rsp = {
                            "code": 0,
                            "message": "Elementos agregados correctamente a la lista"
                        }
                        return JSONResponse(status_code=200, content=rsp)
                error_messages.append(f"No se encuentra la regla específica {key_rule} para la columna {name_column}")
        
        if error_messages:
            return JSONResponse(status_code=404, content={"code": 1, "message": error_messages[0]})
        
        return JSONResponse(status_code=404, content={"code": 1, "message": f"No se encuentra información de la columna {name_column}"})

    except Exception as ex:
        print('Error en add_rules_especific_by_name_rule_etl: ', ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error - {ex}"})


# ====================================================================================== #
#  R E M O V E   C O N T E N T  R U L E S   S P E C I F I C    B Y   N A M E   R U L E   #
# ====================================================================================== #

def remove_content_rules_from_specific_rule(specific_rule, elements):
    for element in elements:
        specific_rule[:] = [item for item in specific_rule if item.lower() != element.lower()]


def remove_content_rules_specific_by_name_rule_etl(name_file, name_column, key_rule, elements: List[str]):
    try:
        error_messages = []
        if not elements:
            return JSONResponse(status_code=400, content={"code": 1, "message": "La lista de elementos no puede estar vacía"})

        with open(config.PATH_PARAMETERS_JSON, 'r') as file:
            data = json.load(file)

        if name_file not in data["configuration_validation"]:
            return JSONResponse(status_code=404, content={"code": 1, "message": f"No se encuentra información del archivo {name_file}"})

        for rule in data["configuration_validation"][name_file]["rules_validations"]:
            if rule["column_validate"] == name_column:
                for s_rule in rule["rules"]["specific_rules"]:
                    if s_rule["specific_rule_type"] == key_rule:
                        rule_property = s_rule["rule_property"]
                        remove_content_rules_from_specific_rule(rule_property, elements)
                        save_data_to_json(data)
                        rsp = {
                            "code": 0,
                            "message": "Elementos eliminados correctamente de la lista"
                        }
                        return JSONResponse(status_code=200, content=rsp)
                error_messages.append(f"No se encuentra la regla específica {key_rule} para la columna {name_column}")

        if error_messages:
            return JSONResponse(status_code=404, content={"code": 1, "message": error_messages[0]})

        return JSONResponse(status_code=404, content={"code": 1, "message": f"No se encuentra información de la columna {name_column}"})

    except Exception as ex:
        print('Error en remove_rules_especific_by_name_rule_etl: ', ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error - {ex}"})
    

# ======================================================================= #
#  R E M O V E    R U L E S   S P E C I F I C    B Y   N A M E   R U L E  #
# ======================================================================= #

def remove_rule_specific_by_name_rule_etl(name_file, name_column, key_rule):
    try:
        with open(config.PATH_PARAMETERS_JSON, 'r') as file:
            data = json.load(file)

        if name_file not in data["configuration_validation"]:
            return JSONResponse(status_code=404, content={"code": 1, "message": f"No se encuentra información del archivo {name_file}"})

        for rule in data["configuration_validation"][name_file]["rules_validations"]:
            if rule["column_validate"] == name_column:
                specific_rules = rule["rules"]["specific_rules"]
                specific_rules[:] = [r for r in specific_rules if r.get("specific_rule_type") != key_rule]
                save_data_to_json(data)
                return JSONResponse(status_code=200, content={"code": 0, "message": "Elementos eliminados correctamente de la lista"})

        return JSONResponse(status_code=404, content={"code": 1, "message": f"No se encuentra información de la columna {name_column}"})

    except Exception as ex:
        print('Error en remove_rules_specific_by_name_rule_etl: ', ex)
        return JSONResponse(status_code=500, content={"code": 1, "message": f"Error - {ex}"})
