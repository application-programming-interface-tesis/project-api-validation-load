import re
from pydantic import BaseModel, conint, EmailStr, constr, Field
from typing import Optional, List
from enum import Enum

INVALID_MAIL = 'Invalid Companyec email'
INVALID_FORMAT = 'Invalid email format'

def validate_email(email: str) -> bool:
    return email.lower().endswith("@companyec.com")

def validate_email_format(email: str) -> bool:
    return bool(re.match(r"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$", email))

def common_email_validations(value):
    if not validate_email(value):
        raise ValueError(INVALID_MAIL)
    if not validate_email_format(value):
        raise ValueError(INVALID_FORMAT)
    return value
  

        
# ====================================== #
#  P A R A M E T E R S   R E Q U E S T   #
# ====================================== # 

class ParametersRequest(BaseModel):
    name_file: str = Field(..., min_length=1)
    date_proc: str = Field(..., min_length=1)
    

# ================================= #
#  Q U E R Y  S T A T U S  F I L E  #
# ================================= #

class TypePhaseStatusFile(str, Enum):
    VALIDATE = 'VALIDATE'
    UPLOAD = 'UPLOAD'
    
class StatusFile(BaseModel):
    name_file: str = Field(..., min_length=1)
    phase: TypePhaseStatusFile

# ================================== #
# C O N F I G U R A T I O N  F I L E #
# ================================== #

class ConfigurationFile(BaseModel):
    name_configuration: str = Field(..., min_length=1)
    
class DeleteConfigFile(BaseModel):
    name_configuration: str = Field(..., min_length=1)
    
class ConfigurationFileByName(BaseModel):
    name_file: str = Field(..., min_length=1)
    
class TypeRule(str, Enum):
    BASIC = "B"
    SPECIFIC = "E"    
    
class FilterNameColumn(BaseModel):
    name_file: str = Field(..., min_length=1)
    name_column: str = Field(..., min_length=1)
    type_rule: TypeRule
    
class FilterNameFile(BaseModel):
    name_file: str = Field(..., min_length=1)
    type_rule: TypeRule
    
    
# ======================================= #
#          A D D    C O L U M N S         #
# ======================================= #

class AddColumns(BaseModel):
    list_columns: List[str]
    
    
# ======================================= #
#        Q U E R Y    C O L U M N S       #
# ======================================= #
    
class QueryColumns(BaseModel):
    name_file: str = Field(..., min_length=1)
    path_file: str = Field(..., min_length=1)
    

# ============================================================================================= #
# A D D / R E M O V E   C O N T E N T  R U L E S   S P E C I F I C    B Y   N A M E   R U L E   #
# ============================================================================================= #

class ContentRulesSpecific(BaseModel):
    name_file: str = Field(..., min_length=1)
    name_column: str = Field(..., min_length=1)
    name_rule: str = Field(..., min_length=1)
    elements: List[str]
    
# ============================================================================== #
# A D D / R E M O V E   R U L E S   S P E C I F I C    B Y   N A M E   R U L E   #
# ============================================================================== #

class RulesSpecific(BaseModel):
    name_file: str = Field(..., min_length=1)
    name_column: str = Field(..., min_length=1)
    name_rule: str = Field(..., min_length=1)