import os

# class Config:
    
#     USER = 'postgres'
#     PASSWORD = 'admin2023' 
#     HOST = '127.0.0.1' 
#     PORT = '5432'
#     BD_NAME = 'data_repository'
    

class Config():
    PATH_PARAMETERS_JSON = os.environ.get('PATH_PARAMETERS_JSON')
    PATH_LOG_INFORMATION = os.environ.get('PATH_LOG_INFORMATION')
    #SO
    USER1_SERVER_SO = os.environ.get('USER1_SERVER_SO')
    HOST1_SERVER_SO = os.environ.get('HOST1_SERVER_SO')
    PASS1_SERVER_SO = os.environ.get('PASS1_SERVER_SO')
    PORT1_SERVER_SO = os.environ.get('PORT1_SERVER_SO')
    #
    PATH_UPLOAD_FOLDER = os.environ.get('PATH_UPLOAD_FOLDER')
    PATH_BASE = os.environ.get('PATH_BASE')
    #BD
    BD_HOST = os.environ.get('BD_HOST')
    BD_PORT =os.environ.get('BD_PORT')
    BD_DATABASE = os.environ.get('BD_DATABASE')
    BD_USER = os.environ.get('BD_USER')
    BD_PASSWORD = os.environ.get('BD_PASSWORD')
    #
    TMP_SCHEME = os.environ.get('TMP_SCHEME')
    ALIAS_TMP = os.environ.get('ALIAS_TMP')

#ARCHIVO PRINCIPAL
# class Config():
#     PATH_PARAMETERS_JSON = 'app/config/parameters.json'
#     PATH_LOG_INFORMATION = 'app/config/log.json'
#     #SO
#     USER1_SERVER_SO = 'reception_files'
#     HOST1_SERVER_SO = 'localhost'
#     PASS1_SERVER_SO = 'Dsup-yNi9AXNH7cd7FW'
#     PORT1_SERVER_SO = "2222"
#     #
#     PATH_UPLOAD_FOLDER = '/datos/reception_files/INPUT/'
#     #BD
#     BD_HOST = "localhost"
#     BD_PORT = "5455"
#     BD_DATABASE = "data_bd"
#     #
#     BD_USER = "data_integration"
#     BD_PASSWORD = "44JLhpZu6MJrDKGZ4"
#     TMP_SCHEME = "tstorage"
#     ALIAS_TMP = "tmp_transfer"
    