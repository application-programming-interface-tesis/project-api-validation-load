import os
import pytz
import time
import glob
import json
import paramiko
from paramiko import ssh_exception
import pandas as pd
import logging
from datetime import datetime
from app.config.config import Config
from app.database.connection import connection_bd
from app.models.request import TypePhaseStatusFile

ANIO_MES_DIA_HHMMSS = "%Y-%m-%d %H:%M:%S"
config = Config()
horario_local = datetime.now(pytz.timezone('America/Guayaquil'))

def load_config_user(ruta_archivo):
    try:
        with open(ruta_archivo, 'r') as archivo:
            configuracion = json.load(archivo)
        return configuracion
    except FileNotFoundError:
        print(f"Error: El archivo '{ruta_archivo}' no fue encontrado.")
        return None
    except json.JSONDecodeError:
        print("Error: No se pudo decodificar el contenido del archivo JSON.")
        return None
    
#=================================#
#   FUNCIONES VALIDATOR FILE      #
#=================================#

def validate_file_path(path_validate, file_validate):
    print('path_validate: ', path_validate)
    print('file_validate: ', file_validate)
    return True if os.path.isdir(path_validate) and os.path.exists(file_validate) else False


#OBTENER TOTAL Y NOMBRE DE LAS COLUMNAS
def get_file_column_information(df):
    # TOTAL Y NOMBRES #
    return df.shape[0], df.shape[1], df.columns.to_list()

#ELIMINAR LOS SALTOS DE LINEA
def remove_line_breaks(df):
    df_copy = df.copy()
    df_copy.replace(to_replace="\n+", value="", regex=True, inplace=True)
    return df_copy


def move_backup_file(path_current, new_path):
    try:
        os.replace(path_current, new_path)
        print('Archivo respaldado en la ruta: ', new_path)
    except Exception as ef:
        print('Error al mover el archivo: ', ef)

def convert_df_to_csv(df, file_load_csv):
    df.to_csv(file_load_csv, sep='|', index=False, header=True, float_format='%.2f', date_format="%Y-%m-%d %H:%M:%S" )
    #df_data_load.to_csv(file_load_csv, sep='|', index=False, header=False, float_format='%.2f', date_format="%Y-%m-%d")
    #df_data_load.to_csv(file_load_csv, sep='|', index=False, header=False, float_format=None, date_format="%Y-%m-%d")


def update_header_dataframe(list_columns_df, tmp_df):
    for hd in range(len(list_columns_df)):
        normalized_header = list_columns_df[hd].upper().strip()
        normalized_header = normalized_header.replace(' ', '_').replace('Ã‘', 'N')
        list_columns_df[hd] = normalized_header
        
    df = tmp_df.set_axis(list_columns_df, axis='columns')   
    return df

#OBTENER NOMBRE ARCHIVO Y EXTENSION
def get_file_name_extension(validate_path):
    root, extension_file = os.path.splitext(validate_path)
    base_name = os.path.basename(validate_path)
    file_name = os.path.splitext(base_name)[0]
    return file_name, extension_file.replace('.', '')


#VALIDAR SI ESTA VACIO EL REGISTRO
def is_value_empty(row_number, register):
    dict_ax={}
    if (len(register) == 0) or (register == ' '):
        return dict_ax
    else:
        dict_ax = generate_dict_error_file(int(row_number) + 2, str(register), 'Celda debe estar vacia')
        logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: '+ str(dict_ax))
        return dict_ax
    

#GENERA DICIONARIO DE ERRORES DE LA FILA
def generate_dict_error_file(fila, registro, error):
    dict_error={}
    dict_error={'fila':'NA', 'registro':'NA', 'error':'NA'}    
    dict_error['fila'] = fila
    dict_error['registro'] = registro
    dict_error['error'] = error
    
    return dict_error
    
def delete_file_csv(path):
    lista_archivos = glob.glob(path + '*.csv', recursive=False)
    for f in lista_archivos:
        os.remove(f)
        print(f'Archivo {f} eliminado exitosamente')
        

def delete_table_temporary(nombre_tabla_temp):
    with connection_bd(config.BD_HOST, 
                       config.BD_PORT, 
                       config.BD_DATABASE, 
                       config.BD_USER, 
                       config.BD_PASSWORD) as session:
            cursor = session.cursor()
            cursor.execute(f"DROP TABLE IF EXISTS {nombre_tabla_temp}")


#OBTENER DATAFRAME
def get_dataframe(ruta_archivo, extension_archivo, cabecera_archivo=True, delimitador="|"):
    if extension_archivo == 'xlsx':
        df = pd.read_excel(ruta_archivo, engine="openpyxl", dtype=str)
    elif extension_archivo == 'xls':
        df = pd.read_excel(ruta_archivo, dtype=str)
    elif extension_archivo == 'txt':
        df = pd.read_csv(ruta_archivo, delimiter=delimitador, dtype=str)
    else:
        if cabecera_archivo is True:
            df = pd.read_csv(ruta_archivo, delimiter=delimitador, dtype=str)
        else:
            df = pd.read_csv(ruta_archivo, delimiter=delimitador, header=None, dtype=str)
            name_columns = [f"COLUMNA_{i+1}" for i in range(len(df.columns))]
            df.columns = name_columns
    #  QUITAR LOS VALORES NaN o ESPACIOS  #
    df.fillna('', inplace = True)
    df.replace(' ', '', inplace = True)
    return df


#OBTENER EL FORMATO DE LA FECHA
def get_file_date_format(name_patter, strdate):
    file_date_format = name_patter.split('_')[-1]
    if file_date_format == 'MMYYYY':
        month = strdate[4:6]
        year = strdate[:4]
        return month + year
    
    elif file_date_format == 'DDMMYYYY':
        day = strdate[6:]
        month = strdate[4:6]
        year = strdate[:4]
        return day + month + year
    
    elif file_date_format == 'BBYYYY':
        month = strdate[4:6]
        year = strdate[:4]
        list_month = ['ENE-1','FEB-2','MAR-3','ABR-4','MAY-5','JUN-6','JUL-7','AGO-8','SEP-9','OCT-10','NOV-11','DIC-12']
        month_letter = ""
        month_aux = []
        for r in range(len(list_month)):
            month_aux = list_month[r].split('-')
            if int(month) == int(month_aux[1]):
                month_letter = month_aux[0]
        
        return month_letter + year
    
    elif file_date_format == 'DDMMYYYY-DDMMYYYY':
        day = strdate[6:]
        month = strdate[4:6]
        year = strdate[:4]
        return '01' + month + year +'_'+ day + month + year
    
#CONSULTAR SI LAS CABECERAS SON IGUALES
def validate_file_headers(file_headers, cgf_headers):
    
    ax_file_headers = sorted(file_headers)
    ax_cgf_headers = sorted(cgf_headers)
    try:
        for w in range(len(ax_cgf_headers)):
            if ax_cgf_headers[w] != ax_file_headers[w]:
                logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: No coincide la columna: ' + ax_file_headers[w])
                return False
    except ValueError as e:
        print('Error: ', e)
        return False
    
    print('Columnas coinciden correctamente')
    return True


#VALIDAR EL CONTENIDO DEL REGISTRO - LISTA
def has_required_value(register, busqueda_filtro, convert_upper):
    aux_register = str(register).rstrip()
    #VALIDAR SI SE APLICA MAYUSCULA
    if convert_upper:
        aux_register = aux_register.upper()
    return True if aux_register == busqueda_filtro else False

    
def remove_file(path_file, days, type_file):
    ANIO_MES_DIA_HHMMSS = "%Y-%m-%d %H:%M:%S"
    ANIO_MES_DIA = "%Y-%m-%d"
    date_current = datetime.strptime(time.strftime(ANIO_MES_DIA), ANIO_MES_DIA)
    files = glob.glob(path_file+'*'+type_file , recursive=False)
    for filename in files:
        file_creation_date = os.path.getctime(filename)
        date_file = time.strftime(ANIO_MES_DIA, time.strptime(time.ctime(file_creation_date)))
        date_creation = datetime.strptime(date_file,ANIO_MES_DIA)
        time_elapsed = date_current - date_creation
        if time_elapsed.days >= days:
            try:
                os.remove(filename)
                logging.info(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ' :: ' + filename + 'Eliminado correctamente')
        
            except OSError as e:
                logging.error(horario_local.strftime(ANIO_MES_DIA_HHMMSS) + ':: No se pudo eliminar los archivos de la ruta :: ' + e.strerror) 
                

def execute_file_pgload(session, file_path, table_name, delimiter='|') -> int:
    try:
        print('Ingreso al metodo..')
        cursor = session.cursor()
        copy_sql = f"COPY {table_name} FROM STDIN WITH CSV HEADER DELIMITER '{delimiter}'"
        
        # Abre el archivo para lectura binaria
        with open(file_path, 'rb') as file:
            print('Se ejecuta el comando..')
            #cursor.copy_expert(sql.SQL(copy_sql), file, delimiter=delimiter)
            cursor.copy_expert(copy_sql, file)
        
        session.commit()
        print(f"Datos copiados exitosamente a la tabla {table_name}")
        return 0
    except Exception as e:
        print(f"Error durante la carga de datos: {e}")
        print("Detalles adicionales:")
        import traceback
        traceback.print_exc()
        return 1
    
    
def connection_server_ubuntu():
    try:
        
        print('HOST1_SERVER_SO: ', config.HOST1_SERVER_SO)
        print('PORT1_SERVER_SO: ', config.PORT1_SERVER_SO)
        print('USER1_SERVER_SO: ', config.USER1_SERVER_SO)
        print('PASS1_SERVER_SO: ', config.PASS1_SERVER_SO)
        
        transport = paramiko.Transport((config.HOST1_SERVER_SO, int(config.PORT1_SERVER_SO)))
        transport.connect(username=config.USER1_SERVER_SO, password=config.PASS1_SERVER_SO)
        return transport
    
    except ssh_exception.AuthenticationException as ex1:
        print('Error de autenticación al conectar al servidor: ', ex1)
        raise
    
    except ssh_exception.SSHException as ex2:
        print('Error SSH al conectar al servidor: ', ex2)
        raise
        
    except Exception as ex:
        print('Error en connection_server_ubuntu: ', ex)
        raise
    

def update_log_file(key, phase: TypePhaseStatusFile, status, message_status):
    with open(config.PATH_LOG_INFORMATION, 'r+') as file:
        data = json.load(file)
        
        if key in data["log_information"]:
            if phase == "VALIDATE":
                data["log_information"][key]["validation_phase"]["validated"] = status
                data["log_information"][key]["validation_phase"]["validated_status"] = message_status
                data["log_information"][key]["validation_phase"]["validation_date"] = horario_local.strftime("%d-%m-%Y %H:%M:%S")
                
                data["log_information"][key]["loading_phase"]["uploaded"] = False
                data["log_information"][key]["loading_phase"]["loaded_status"] = "PENDIENTE"
                data["log_information"][key]["loading_phase"]["upload_date"] = ""
                
                file.seek(0)  
                json.dump(data, file, indent=4)  
                file.truncate()
                print('Actualizacion de estados realizada.')
            
            #if phase == "UPLOAD":
            else:
                data["log_information"][key]["loading_phase"]["uploaded"] = status
                data["log_information"][key]["loading_phase"]["loaded_status"] = message_status
                data["log_information"][key]["loading_phase"]["upload_date"] = horario_local.strftime("%d-%m-%Y %H:%M:%S")
                
                file.seek(0)  
                json.dump(data, file, indent=4)  
                file.truncate()
                print('Actualizacion de estados realizada.')
            
            # file.seek(0)  
            # json.dump(data, file, indent=4)  
            # file.truncate()
            # print('Actualizacion de estados realizada.')
                

def add_template_log_info(section_name_log):
    try:
        with open(config.PATH_PARAMETERS_JSON, 'r') as json_file:
            log_json = json.load(json_file)

        new_log = {
            section_name_log: {
                "validation_phase": {
                    "validated": False,
                    "validated_status": "PENDIENTE",
                    "validation_date": ""
                },
                "loading_phase": {
                    "uploaded": False,
                    "loaded_status": "PENDIENTE",
                    "upload_date": ""
                }
            }
        }

        log_json["configuration_validation"].update(new_log)
        return 0
    except Exception as ex:
        print('Error en add_new_section: ', ex)
        return 1


def delete_section_log_info(section_name_log):
    try:
        with open(config.PATH_PARAMETERS_JSON, 'r') as json_file:
            data = json.load(json_file)
        
        if section_name_log in data["configuration_validation"]:
            del data["configuration_validation"][section_name_log]
        
        with open(config.PATH_PARAMETERS_JSON, 'w') as json_file:
            json.dump(data, json_file, indent=4)
        
        return 0 
    except Exception as ex:
        print('Error en delete_section_log_info: ', ex)
        return 1