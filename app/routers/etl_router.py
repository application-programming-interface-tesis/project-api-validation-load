from fastapi import APIRouter, UploadFile, File, Body
from app.models.request import (AddColumns, 
                                ContentRulesSpecific, 
                                DeleteConfigFile, 
                                FilterNameColumn, 
                                FilterNameFile, 
                                ParametersRequest, 
                                ConfigurationFile, 
                                QueryColumns, 
                                RulesSpecific, 
                                StatusFile, TypeRule)
from app.services.configuration_file import (add_columns_configuration_file_etl, 
                                             add_content_rules_specific_by_name_rule_etl, 
                                             configuration_file_etl, 
                                             query_columns_configured_by_name_etl, 
                                             query_configuration_by_name_etl, 
                                             query_configuration_file_column_etl,  
                                             query_rules_configured_by_name_etl,
                                             query_rules_filter_by_name_etl, 
                                             remove_configuration_file_etl, 
                                             remove_content_rules_specific_by_name_rule_etl, 
                                             remove_rule_specific_by_name_rule_etl, 
                                             rules_filter_by_name_column_etl)
from app.services.information_rules import get_information_type_rules_etl
from app.services.load_file_server import get_information_file_etl, get_list_information_file_etl, load_file_copy_server_etl, load_file_server_etl, remove_list_file_name_etl
from app.services.query_status_file import query_status_file_all_etl, query_status_file_filter_etl, query_status_file_filter_name_file_etl
from app.services.upload_file import upload_file_etl
from app.services.validator_file import validator_file_etl


etl = APIRouter()


# ================================= #
#   V A L I D A T O R   F I L E     #
# ================================= # 

@etl.post("/etl/validator_file", tags=["Validator File"], response_model=dict, status_code=200,
          summary="Validador de Archivo",
          description="Método para validar un archivo previa su carga la base de datos")
def validator_file_etl_router(validator_file: ParametersRequest) -> dict:
        return validator_file_etl(validator_file.name_file, validator_file.date_proc)


# ================================= #
#       U P L O A D   F I L E       #
# ================================= #

@etl.post("/etl/upload_file", tags=["Upload File"], response_model=dict, status_code=200,
          summary="Carga de Archivo",
          description="Método para cargar el archivo a la base de datos")
def upload_file_etl_router(upload_file: ParametersRequest) -> dict:
        return upload_file_etl(upload_file.name_file, upload_file.date_proc)


# ================================= #
# U P L O A D  F I L E  S E R V E R #
# ================================= #

@etl.post("/etl/load_file_server", tags=["Load File Server"], response_model=dict, status_code=200,
          summary="Subir archivo al servidor",
          description="Método para subir un archivo a una ruta del servidor")
async def load_file_server_router(file: UploadFile = File(...)) -> dict:
        return await load_file_server_etl(file.filename, file)



@etl.post("/etl/load_file_server2", tags=["Load File Server"], response_model=dict, status_code=200,
          summary="Subir archivo al servidor",
          description="Método para subir un archivo a una ruta del servidor")
def load_file_copy_server_router(file: UploadFile = File(...)) -> dict:
        return load_file_copy_server_etl(file)



# ============================================ #
#   G E T   I N F O R M A T I O N    F I L E   #
# ============================================ #

@etl.post("/etl/information_file", tags=["Information File"], response_model=dict, status_code=200,
          summary="Obtener información de un archivo en linea",
          description="Método para obtener información de un archivo on-line")
async def get_information_file_router(file: UploadFile = File(...)) -> dict:
        return await get_information_file_etl(file)


# ==================================================== #
#   G E T   L I S T   F I L E  I N F O R M A T I O N   #
# ==================================================== #

@etl.get("/etl/list_file/information_file", tags=["Information File"], response_model=dict, status_code=200,
          summary="Obtener información de los archivo subidos en la ruta on-line",
          description="Método para obtener información de los archivo subidos en la ruta on-line")
async def get_list_information_file_router():
        return await get_list_information_file_etl()

# ========================================================== #
#   R E M O V E   L I S T   F I L E  I N F O R M A T I O N   #
# ========================================================== #

@etl.delete("/etl/list_file/remove/file_name", tags=["Information File"], response_model=dict, status_code=200,
            summary="Eliminar un archivo de la ruta del servidor",
            description="Método para eliminar un archivo de la ruta del servidor")
def remove_list_file_name_router(name_file: str):
        return remove_list_file_name_etl(name_file)



# ================================================ #
#  Q U E R Y  S T A T U S  F I L T E R   F I L E   #
# ================================================ #

@etl.post("/elt/query_status_file/filter", tags=["Query Status File"], response_model=dict, status_code=200,
         summary="Consultar el estado de un archivo filtrando su fase",
         description="Método para consultar el estado de fase de un archivo")
def query_status_file_router(filter: StatusFile):
        return query_status_file_filter_etl(filter.name_file, filter.phase)


# ======================================================== #
#  Q U E R Y  S T A T U S  F I L T E R   N A M E  F I L E  #
# ======================================================== #

@etl.get("/elt/query_status_file/filter_name", tags=["Query Status File"], response_model=dict, status_code=200,
         summary="Consultar el estado de un archivo filtrando el nombre",
         description="Método para consultar el estado de un archivo filtrando el nombre")
def query_status_file_router(name_file: str):
        return query_status_file_filter_name_file_etl(name_file)


# ========================================= #
#  Q U E R Y  S T A T U S  A L L   F I L E  #
# ========================================= #

@etl.get("/elt/query_status_file/all", tags=["Query Status File"], response_model=dict, status_code=200,
         summary="Consultar todos los estados de los archivos",
         description="Método para consultar los estados de los archivos")
def query_status_file_router():
        return query_status_file_all_etl()


# ================================== #
# C O N F I G U R A T I O N  F I L E #
# ================================== #

# ================================= #
#  I N F O R M A T I O N   R U L E  #
# ================================= # 

@etl.get("/etl/configuration_file/information/type_rule", tags=["Information Configuration"], response_model=dict,
         status_code=200,
         summary="Consultar la información de las reglas",
         description="Método para consultar la información de las reglas")
def get_information_type_rules_router(type: TypeRule):
        return get_information_type_rules_etl(type)


# ===================================================== #
# Q U E R Y   C O N F I G U R A T I O N   B Y   N A M E #
# ===================================================== #

@etl.get("/etl/configuration_file/query_configuration/name_file", tags=["View Configuration File By Filter"], response_model=dict,
         status_code=200,
         summary="Consultar la configuración general de un archivo",
         description="Método para Consultar la configuración general de un archivo")
def query_configuration_by_name_router(name_file: str) -> dict:
        return query_configuration_by_name_etl(name_file)


# ========================================= #
# Q U E R Y   C O L U M N S   B Y   N A M E #
# ========================================= #

@etl.get("/etl/configuration_file/query_columns/name_file", tags=["View Configuration File By Filter"], response_model=dict,
         status_code=200,
         summary="Consultar las columnas configuradas de un archivo",
         description="Método para consultar columnas configuradas de un archivo")
def query_columns_configured_by_name_router(name_file: str) -> dict:
        return query_columns_configured_by_name_etl(name_file)


# ======================================= #
# Q U E R Y   R U L E S    B Y    N A M E #
# ======================================= #

@etl.get("/etl/configuration_file/query_rules/name_file", tags=["View Configuration File By Filter"], response_model=dict,
         status_code=200,
         summary="Consultar las reglas generales de un archivo configurado",
         description="Método para consultar reglas generales configuradas de un archivo")
def query_rules_configured_by_name_router(name_file: str) -> dict:
        return query_rules_configured_by_name_etl(name_file)


# ============================================================================= #
# Q U E R Y   R U L E S   S P E C I F I C / B A S I C   B Y   N A M E   F I L E #
# ============================================================================= #

@etl.post("/etl/configuration_file/query_rules/name_file", tags=["View Configuration File By Filter"],
         response_model=dict,
         status_code=200,
         summary="Consultar las reglas basicas o especificas de un archivo configurado",
         description="Método para consultar reglas basicas o especificas configuradas de un archivo")
def query_rules_filter_by_name_router(filter: FilterNameFile) -> dict:
        return query_rules_filter_by_name_etl(filter.name_file, filter.type_rule)


# ================================================================================= #
# Q U E R Y   R U L E S   S P E C I F I C / B A S I C   B Y   N A M E   C O L U M N #
# ================================================================================= #

@etl.post("/etl/configuration_file/query_rules/name_column", tags=["View Configuration File By Filter"], response_model=dict,
        status_code=200,
        summary="Consultar las reglas basicas o especificas de la columna de un archivo configurado",
        description="Método para consultar las reglas basicas o especificas de la columna de un archivo configurado")
def rules_filter_by_name_column_router(filter: FilterNameColumn) -> dict:
        return rules_filter_by_name_column_etl(filter.name_file, filter.name_column.upper(), filter.type_rule)


# =========================================== #
# A D D   C O N F I G U R A T I O N   F I L E #
# =========================================== #

@etl.post("/etl/configuration_file/add_template", tags=["Configuration File"], response_model=dict, status_code=200,
         summary="Agregar plantilla de configuración de reglas",
         description="Método para agregar plantilla de configuración de reglas")
def configuration_file_router(element: ConfigurationFile) -> dict:
        return configuration_file_etl(element.name_configuration)


# ======================================= #
# D E L E T E  C O N F I G U R A T I O N  #
# ======================================= #

@etl.delete("/etl/configuration_file/remove_template", tags=["Configuration File"], response_model=dict, status_code=200,
          summary="Eliminar configuración de reglas",
          description="Método para eliminar la configuración de reglas")
def remove_configuration_file_router(element: DeleteConfigFile) -> dict:
        return remove_configuration_file_etl(element.name_configuration)


# =============================================================================== #
# A D D  C O N T E N T   R U L E S   S P E C I F I C    B Y   N A M E   R U L E   #
# =============================================================================== #

@etl.post("/etl/configuration_file/add_rules/content/name_rule", tags=["Configuration File"], response_model=dict, status_code=200,
          summary="Agregar contenido a una regla específicas mediante el nombre de la regla",
          description="Método para agregar contenido a una regla específicas mediante el nombre de la regla")
def add_content_rule_specific_by_name_rule_router(n: ContentRulesSpecific) -> dict:
        return add_content_rules_specific_by_name_rule_etl(n.name_file, n.name_column, n.name_rule, n.elements)


# ====================================================================================== #
#  R E M O V E   C O N T E N T  R U L E S   S P E C I F I C    B Y   N A M E   R U L E   #
# ====================================================================================== #

@etl.delete("/etl/configuration_file/remove_rules/content/name_rule", tags=["Configuration File"], response_model=dict, status_code=200,
            summary="Eliminar contenido de una regla específicas mediante el nombre de la regla",
            description="Método para eliminar contenido de una regla específicas mediante el nombre de la regla")
def remove_content_rule_specific_by_name_rule_router(n: ContentRulesSpecific) -> dict:
        return remove_content_rules_specific_by_name_rule_etl(n.name_file, n.name_column, n.name_rule, n.elements)


# ======================================================================= #
#  R E M O V E    R U L E S   S P E C I F I C    B Y   N A M E   R U L E  #
# ======================================================================= #

@etl.delete("/etl/configuration_file/remove_rules/name_rule", tags=["Configuration File"], response_model=dict,
            status_code=200,
            summary="Eliminar una regla específicas mediante el nombre de la regla",
            description="Método para eliminar una regla específicas mediante el nombre de la regla")
def remove_rule_specific_by_name_rule_router(n: RulesSpecific) -> dict:
        return remove_rule_specific_by_name_rule_etl(n.name_file, n.name_column, n.name_rule)




# ======================================= #
#        Q U E R Y    C O L U M N S       #
# ======================================= #

@etl.post("/etl/configuration_file/query_columns", tags=["Configuration File"], response_model=dict, status_code=200,
          summary="Consultar las columnas de un archivo",
          description="Método para consultar las columnas de un archivo")
def query_configuration_file_column_router(element: QueryColumns) -> dict:
        return query_configuration_file_column_etl(element.name_file, element.path_file)


# ======================================= #
#          A D D    C O L U M N S         #
# ======================================= #

@etl.post("/etl/configuration_file/add_columns", tags=["Configuration File"], response_model=dict, status_code=200,
          summary="Agregar columnas en la configuracion de las reglas",
          description="Método para agregar columnas en las reglas de configuracion")
def add_columns_configuration_file_router(element: AddColumns) -> dict:
        return add_columns_configuration_file_etl(element.list_columns)


