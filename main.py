#!/usr/bin/env python
# coding: utf-8

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from app.middlewares.error_handler import ErrorHandler
from app.routers import etl_router

app = FastAPI()
app.title = "API Validator and Automatic File Upload to a Database"
app.version = "0.0.1"

# Configurar CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # O especifica los dominios permitidos
    allow_credentials=True,
    allow_methods=["*"],  # O especifica los métodos permitidos
    allow_headers=["*"],  # O especifica los encabezados permitidos
)

app.include_router(etl_router.etl)
app.add_middleware(ErrorHandler)

if __name__ == "__main__":
    import uvicorn
    
    uvicorn.run(app, host="0.0.0.0", port=8000)
